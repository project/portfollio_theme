<?php

/**
 * @file
 * Custom setting for Resume theme.
 */

function Resume_form_system_theme_settings_alter(&$form, &$form_state) {
  $form['Resume'] = [
    '#type'       => 'vertical_tabs',
    '#title'      => '<h3>' . t('Resume Theme Settings') . '</h3>',
    '#default_tab' => 'general',
  ];

  /**
   * Main Tabs.
   */
  // Settings under social tab.
  // Show or hide all icons.
  $form['social']['social_profile'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Social Profile'),
  ];
  $form['social']['social_profile']['all_icons'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Show Social Icons'),
  ];

  $form['social']['social_profile']['all_icons']['all_icons_show'] = [
    '#type'          => 'checkbox',
    '#title'         => t('Show social icons in footer'),
    '#default_value' => theme_get_setting('all_icons_show', 'Resume'),
    '#description'   => t("Check this option to show social icons in footer. Uncheck to hide."),
  ];

  // Facebook.
    $form['social']['social_profile']['facebook'] = [
    '#type'        => 'details',
    '#title'       => t("Facebook"),
  ];
  $form['social']['social_profile']['facebook']['facebook_url'] = [
    '#type'          => 'textfield',
    '#title'         => t('Facebook Url'),
    '#description'   => t("Enter yours facebook profile or page url. Leave the url field blank to hide this icon."),
    '#default_value' => theme_get_setting('facebook_url', 'Resume'),
  ];
  
// Twitter.
$form['social']['social_profile']['twitter'] = [
  '#type'        => 'details',
  '#title'       => t("Twitter"),
];

$form['social']['social_profile']['twitter']['twitter_url'] = [
  '#type'          => 'textfield',
  '#title'         => t('Twitter Url'),
  '#description'   => t("Enter yours twitter page url. Leave the url field blank to hide this icon."),
  '#default_value' => theme_get_setting('twitter_url', 'zuvi'),
];

// Instagram.
$form['social']['social_profile']['instagram'] = [
  '#type'        => 'details',
  '#title'       => t("Instagram"),
];

$form['social']['social_profile']['instagram']['instagram_url'] = [
  '#type'          => 'textfield',
  '#title'         => t('Instagram Url'),
  '#description'   => t("Enter yours instagram page url. Leave the url field blank to hide this icon."),
  '#default_value' => theme_get_setting('instagram_url', 'zuvi'),
];

// Linkedin.
$form['social']['social_profile']['linkedin'] = [
  '#type'        => 'details',
  '#title'       => t("Linkedin"),
];

$form['social']['social_profile']['linkedin']['linkedin_url'] = [
  '#type'          => 'textfield',
  '#title'         => t('Linkedin Url'),
  '#description'   => t("Enter yours linkedin page url. Leave the url field blank to hide this icon."),
  '#default_value' => theme_get_setting('linkedin_url', 'zuvi'),
];

// YouTube.
$form['social']['social_profile']['youtube'] = [
  '#type'        => 'details',
  '#title'       => t("YouTube"),
];

$form['social']['social_profile']['youtube']['youtube_url'] = [
  '#type'          => 'textfield',
  '#title'         => t('YouTube Url'),
  '#description'   => t("Enter yours youtube.com page url. Leave the url field blank to hide this icon."),
  '#default_value' => theme_get_setting('youtube_url', 'zuvi'),
];


}
